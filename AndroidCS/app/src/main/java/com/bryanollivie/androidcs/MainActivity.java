package com.bryanollivie.androidcs;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.bryanollivie.androidcs.activity.DetailRepositoryActivity;
import com.bryanollivie.androidcs.adapter.RepositoriesAdapter;
import com.bryanollivie.androidcs.apiwebservice.DataAccess;
import com.bryanollivie.androidcs.apiwebservice.DataAccessInterface;
import com.bryanollivie.androidcs.domain.Items;
import com.bryanollivie.androidcs.domain.Repositories;
import com.bryanollivie.androidcs.util.EndlessRecyclerViewScrollListener;
import com.bryanollivie.androidcs.util.ListClickCallback;
import com.bryanollivie.androidcs.util.UiUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, ListClickCallback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.repo_recycleView)
    RecyclerView repo_recycleView;


    @BindView(R.id.no_content)
    LinearLayout no_content;

    @BindView(R.id.coordinator_layout_main)
    CoordinatorLayout coordinator_layout_main;


    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    RepositoriesAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        setupRecycleView();
        setupSwipeToRefresh();

    }

    public void setupSwipeToRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                search(0);
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);
    }


    public void setupRecycleView() {
        adapter = new RepositoriesAdapter(this, this);
        repo_recycleView.setHasFixedSize(true);
        repo_recycleView.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager manager = new LinearLayoutManager(repo_recycleView.getContext());
        repo_recycleView.setLayoutManager(manager);
        repo_recycleView.setAdapter(adapter);
        repo_recycleView.addOnScrollListener(new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                search(page);
            }
        });
    }

    public void search(int page) {

        swipeRefreshLayout.setRefreshing(true);

        DataAccess.getInstance().getRepositoriesList(page, new DataAccessInterface() {
            @Override
            public void success(Object data, int code) {
                Repositories repositories = (Repositories) data;
                List<Items> repositoriesLisItems = null;

                if (repositories != null) {
                    repositoriesLisItems = repositories.getItems();


                    adapter.addRepositories(repositoriesLisItems);
                    reload();

                    if (swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    } else {
                        swipeRefreshLayout.setRefreshing(true);
                    }
                }
            }

            @Override
            public void failed() {
                no_content.setVisibility(View.VISIBLE);
                UiUtil.showSnackBar(coordinator_layout_main,R.string.erro_dados,R.color.red_700,Snackbar.LENGTH_LONG);

            }
        });
    }

    public void reload() {
        if (adapter.getRespositories().size() <= 0)
            no_content.setVisibility(View.VISIBLE);
        else
            no_content.setVisibility(View.GONE);

        adapter.notifyDataSetChanged();
        repo_recycleView.refreshDrawableState();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void click(int position) {
        Items repoItem = adapter.getRespositories().get(position);

        Intent intent = new Intent(MainActivity.this, DetailRepositoryActivity.class);
        intent.putExtra("data", repoItem);

        startActivity(intent);


    }
}

