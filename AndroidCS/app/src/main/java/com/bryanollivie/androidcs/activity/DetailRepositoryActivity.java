package com.bryanollivie.androidcs.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bryanollivie.androidcs.MainActivity;
import com.bryanollivie.androidcs.R;
import com.bryanollivie.androidcs.adapter.PullRequestAdapter;
import com.bryanollivie.androidcs.adapter.RepositoriesAdapter;
import com.bryanollivie.androidcs.apiwebservice.DataAccess;
import com.bryanollivie.androidcs.apiwebservice.DataAccessInterface;
import com.bryanollivie.androidcs.domain.Items;
import com.bryanollivie.androidcs.domain.PullRequest;
import com.bryanollivie.androidcs.domain.Repositories;
import com.bryanollivie.androidcs.util.EndlessRecyclerViewScrollListener;
import com.bryanollivie.androidcs.util.ListClickCallback;
import com.bryanollivie.androidcs.util.UiUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailRepositoryActivity extends AppCompatActivity implements ListClickCallback {

    Items repoItem = null;
    PullRequestAdapter adapter;
    MaterialDialog progress;
    int sizePullResquest;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.pullrequest_list)
    CoordinatorLayout pullrequest_list;

/*
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;*/

/*

    @BindView(R.id.no_content)
    LinearLayout no_content;
*/

    @BindView(R.id.pull_request_recyclerview)
    RecyclerView pull_request_recyclerview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_repository_activity);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        repoItem = (Items) getIntent().getSerializableExtra("data");

        if (repoItem == null)
            finish();
        getSupportActionBar().setTitle(repoItem.getName());
        setupRecycleView();
        //setupSwipeToRefresh();
    }

/*

    public void setupSwipeToRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                search(repoItem.getOwner().getLogin(), repoItem.getName());
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);
    }
*/

    public void setupRecycleView() {
        adapter = new PullRequestAdapter(this,this);
        pull_request_recyclerview.setHasFixedSize(true);
        pull_request_recyclerview.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager manager = new LinearLayoutManager(pull_request_recyclerview.getContext());
        pull_request_recyclerview.setLayoutManager(manager);
        pull_request_recyclerview.setAdapter(adapter);

        search(repoItem.getOwner().getLogin(), repoItem.getName());

    }

    public void search(String criador, String repositorio) {

        progress = UiUtil.showProgress(this,R.string.carregando_dados, progress);

        DataAccess.getInstance().getPullRequestList(criador, repositorio, new DataAccessInterface() {
            @Override
            public void success(Object data, int code) {


                List<PullRequest> pullRequestList = (List<PullRequest>) data;


                adapter.addRepositories(pullRequestList);
                reload();

                if(progress!=null)
                    progress.dismiss();
            }

            @Override
            public void failed() {
                if(progress!=null)
                    progress.dismiss();
                UiUtil.showSnackBar(pullrequest_list,R.string.erro_dados,R.color.red_700,Snackbar.LENGTH_LONG);

            }
        });
    }


    public void reload() {
       /* if (adapter.getPullRequestsList().size() <= 0)
            no_content.setVisibility(View.VISIBLE);
        else
            no_content.setVisibility(View.GONE);*/

        adapter.notifyDataSetChanged();
        pull_request_recyclerview.refreshDrawableState();
    }

    @Override
    public void click(int position) {
        PullRequest pullRequest = adapter.getPullRequestsList().get(position);

        Intent intent = new Intent(DetailRepositoryActivity.this, WebViewActivity.class);
        intent.putExtra("repository", repoItem);
        intent.putExtra("pull_request", pullRequest);
        startActivity(intent);


    }
}
