package com.bryanollivie.androidcs.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bryanollivie.androidcs.R;
import com.bryanollivie.androidcs.domain.Items;
import com.bryanollivie.androidcs.domain.PullRequest;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewActivity extends AppCompatActivity {

    String criador;
    String repositorio;
    int pullRequestId;
    String url;
    Items repoItem = null;
    PullRequest pullRequest = null;
    MaterialDialog progress;


    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @BindView(R.id.webview)
    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);


        pullRequest = (PullRequest) getIntent().getSerializableExtra("pull_request");
        repoItem = (Items) getIntent().getSerializableExtra("repository");


        if (repoItem == null || pullRequest == null)
            finish();

        getSupportActionBar().setTitle(pullRequest.getUser().getLogin());

        criador = repoItem.getOwner().getLogin();
        repositorio = repoItem.getName();
        pullRequestId = pullRequest.getNumber();

        url = "https://github.com/" + criador + "/" + repositorio + "/pull/" + pullRequestId;

        setupWebView();
    }


    public void setupWebView() {

        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        //https://github.com/elastic/elasticsearch/pull/21415
        webview.loadUrl(url);
    }

}
