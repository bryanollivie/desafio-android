package com.bryanollivie.androidcs.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bryanollivie.androidcs.R;
import com.bryanollivie.androidcs.domain.Items;
import com.bryanollivie.androidcs.domain.PullRequest;
import com.bryanollivie.androidcs.util.ListClickCallback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bryanollivie on 08/11/16.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.viewHolder> {

    List<PullRequest> pullRequestsList = new ArrayList();
    ListClickCallback callback;
    Context context;
    public void clear() {
        pullRequestsList.clear();
    }

    public void addRepositories(List<PullRequest> pullRequests) {
        this.pullRequestsList.addAll(pullRequests);
    }

    public List<PullRequest> getPullRequestsList() {
        return pullRequestsList;
    }

    public PullRequestAdapter(Context context, ListClickCallback callback) {
        this.callback = callback;
        this.context = context;
    }


    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pullresquest_list_item, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(final viewHolder holder, final int position) {
        PullRequest pullRequest = pullRequestsList.get(position);

        holder.pull_title.setText(pullRequest.getTitle());
        holder.pull_body.setText(pullRequest.getBody());
        holder.pull_data.setText(pullRequest.getCreated_at());

        holder.user_username.setText(pullRequest.getUser().getLogin());
        if(pullRequest.getUser().getAvatar_url() != null || !pullRequest.getUser().getAvatar_url().isEmpty() ) {
            Picasso.with(context).load(pullRequest.getUser().getAvatar_url() ).into(holder.user_image);
        }else{
            Picasso.with(context).load(R.drawable.github_logo).into(holder.user_image);
        }


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.click(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return pullRequestsList.size();
    }


    public class viewHolder extends RecyclerView.ViewHolder {

        View mView;

        @Nullable
        @BindView(R.id.user_username)
        TextView user_username;

        @Nullable
        @BindView(R.id.pull_data)
        TextView pull_data;

        @Nullable
        @BindView(R.id.user_image)
        ImageView user_image;

        @Nullable
        @BindView(R.id.pull_body)
        TextView pull_body;

        @Nullable
        @BindView(R.id.pull_title)
        TextView pull_title;


        public viewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }
    }

}
