package com.bryanollivie.androidcs.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bryanollivie.androidcs.R;
import com.bryanollivie.androidcs.domain.Items;
import com.bryanollivie.androidcs.domain.Repositories;
import com.bryanollivie.androidcs.util.ListClickCallback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bryanollivie on 08/11/16.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.RepositoriesViewHolder> {

    List<Items> repositories = new ArrayList();
    ListClickCallback callback;

    Context context;

    public void clear() {
        repositories.clear();
    }

    public void addRepositories(List<Items> repo) {
        this.repositories.addAll(repo);
    }

    public List<Items> getRespositories() {
        return repositories;
    }

    public RepositoriesAdapter(Context context, ListClickCallback callback) {
        this.callback = callback;
        this.context = context;
    }


    @Override
    public RepositoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repositories_list, parent, false);
        return new RepositoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RepositoriesViewHolder holder, final int position) {
        Items repo = repositories.get(position);

        holder.autor_username.setText(repo.getOwner().getLogin().toUpperCase());
        //holder.autor_nomesobrenome.setText(repo.getOwner().getLogin());
        holder.repo_nome.setText(repo.getName());
        holder.repo_descricao.setText(repo.getDescription());
        holder.repo_forks.setText("" + repo.getForks_count());
        holder.repo_stars.setText("" + repo.getStargazers_count());

        if(repo.getOwner().getAvatar_url()!=null || !repo.getOwner().getAvatar_url().isEmpty()) {
            Picasso.with(context).load(repo.getOwner().getAvatar_url()).into(holder.autor_image);
            //holder.autor_image.setImageBitmap(repo.getOwner().getAvatar_url());
        }else{
            Picasso.with(context).load(R.drawable.github_logo).into(holder.autor_image);
        }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.click(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }


    public class RepositoriesViewHolder extends RecyclerView.ViewHolder {

        View mView;
/*

        @BindView(R.id.autor_nomesobrenome)
        TextView autor_nomesobrenome;
*/

        @Nullable
        @BindView(R.id.autor_username)
        TextView autor_username;

        @Nullable
        @BindView(R.id.autor_image)
        ImageView autor_image;

        @Nullable
        @BindView(R.id.repo_descricao)
        TextView repo_descricao;

        @Nullable
        @BindView(R.id.repo_nome)
        TextView repo_nome;

        @Nullable
        @BindView(R.id.repo_forks)
        TextView repo_forks;

        @Nullable
        @BindView(R.id.repo_stars)
        TextView repo_stars;

        public RepositoriesViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }
    }

}
