package com.bryanollivie.androidcs.apiwebservice;

import com.bryanollivie.androidcs.domain.PullRequest;
import com.bryanollivie.androidcs.domain.Repositories;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bryanollivie on 08/11/16.
 */

public class DataAccess {


    private static DataAccess instance;

    public static synchronized DataAccess getInstance() {
        if (instance == null) {
            instance = new DataAccess();
        }

        return instance;
    }

    public void getRepositoriesList(int page , final DataAccessInterface callback) {
        ServiceApi.getInstance().getService().getRepositoriesList(page).enqueue(new Callback<Repositories>() {
            @Override
            public void onResponse(Call<Repositories> call, Response<Repositories> response) {
                callback.success(response.body(), response.code());
            }

            @Override
            public void onFailure(Call<Repositories> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getPullRequestList(String criador, String repositorio, final DataAccessInterface callback) {
        ServiceApi.getInstance().getService().getPullRequestList(criador, repositorio).enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                callback.success(response.body(), response.code());
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

}
