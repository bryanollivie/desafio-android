package com.bryanollivie.androidcs.apiwebservice;

/**
 * Created by bryanollivie on 07/11/16.
 */

public interface DataAccessInterface<T> {
    void success(T data, int code);
    void failed();
}

