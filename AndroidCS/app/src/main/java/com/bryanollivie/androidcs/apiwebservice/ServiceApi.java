package com.bryanollivie.androidcs.apiwebservice;

import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by bryanollivie on 07/11/16.
 */

public class ServiceApi {

    private static ServiceApi instance;
    private Retrofit retrofit;
    private ServiceApiInterface service;

    public ServiceApiInterface getService() {
        return service;
    }

    public static synchronized ServiceApi getInstance() {
        if (instance == null) {
            instance = new ServiceApi();
        }

        return instance;
    }

    public ServiceApi() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .client(client)
                .build();

        service = retrofit.create(ServiceApiInterface.class);
    }
}
