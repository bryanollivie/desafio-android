package com.bryanollivie.androidcs.apiwebservice;

import com.bryanollivie.androidcs.domain.PullRequest;
import com.bryanollivie.androidcs.domain.Repositories;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by bryanollivie on 07/11/16.
 */

public interface ServiceApiInterface {

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<Repositories> getRepositoriesList(@Query("page") int page);

    @GET("repos/{criador}/{repositorio}/pulls")
    Call <List<PullRequest>> getPullRequestList(@Path("criador") String criador, @Path("repositorio") String repositorio);

}
