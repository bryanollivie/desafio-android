package com.bryanollivie.androidcs.domain;

import java.io.Serializable;

/**
 * Created by bryanollivie on 07/11/16.
 */

public class Items implements Serializable {

    long id;
    String name;
    String description;
    long forks_count;
    long stargazers_count;
    Owner owner;
    /*
    String privado;
    String html_url;
    String full_name;
    String fork;
    String url;
    String forks_url;
    String keys_url;
    String collaborators_url;
    String teams_url;
    String hooks_url;
    String issue_events_url;
    String events_url;
    String assignees_url;
    String branches_url;
    String blobs_url;
    String tags_url;*/

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getForks_count() {
        return forks_count;
    }

    public void setForks_count(long forks_count) {
        this.forks_count = forks_count;
    }

    public long getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(long stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
