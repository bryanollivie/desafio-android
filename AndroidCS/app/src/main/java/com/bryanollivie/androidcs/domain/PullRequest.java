package com.bryanollivie.androidcs.domain;

import java.io.Serializable;

/**
 * Created by bryanollivie on 08/11/16.
 */

public class PullRequest implements Serializable{

    String title;
    String body;
    UserPullRequest user;
    String created_at;
    int number;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public UserPullRequest getUser() {
        return user;
    }

    public void setUser(UserPullRequest user) {
        this.user = user;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String formatDate(String created_at){

        return "";
    }
}
