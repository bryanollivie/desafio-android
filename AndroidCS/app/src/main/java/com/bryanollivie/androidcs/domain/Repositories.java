package com.bryanollivie.androidcs.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bryanollivie on 07/11/16.
 */

public class Repositories implements Serializable{

     long total_count;
     boolean incomplete_results;
     List<Items> items;

    public long getTotal_count() {
        return total_count;
    }

    public void setTotal_count(long total_count) {
        this.total_count = total_count;
    }

    public boolean isIncomplete_results() {
        return incomplete_results;
    }

    public void setIncomplete_results(boolean incomplete_results) {
        this.incomplete_results = incomplete_results;
    }

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }
}
