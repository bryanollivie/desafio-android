package com.bryanollivie.androidcs.domain;

import java.io.Serializable;

/**
 * Created by bryanollivie on 08/11/16.
 */

public class UserPullRequest implements Serializable {

    String login;
    String avatar_url;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }
}
