package com.bryanollivie.androidcs.util;

/**
 * Created by bryanollivie on 08/11/16.
 */

public interface ListClickCallback {

    void click (int position);

}
