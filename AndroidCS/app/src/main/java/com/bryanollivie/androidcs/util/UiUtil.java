package com.bryanollivie.androidcs.util;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by Bryan Souza on 07/10/2016.
 */

public class UiUtil {

    public static void showSnackBar(View view, int msg, int color, int duration) {
        Snackbar mSnack = Snackbar.make(view, "\n" + view.getContext().getResources().getString(msg) + "\n", duration);
        View viewLayout = mSnack.getView();
        viewLayout.setBackgroundColor(color);
        mSnack.show();
    }

    //Mostrar Progress
    public static MaterialDialog showProgress(Context context, int msg, MaterialDialog progress) {
         progress = new MaterialDialog.Builder(context)
                .progress(true, 0)
                .title(msg)
                .cancelable(false)
                .progressIndeterminateStyle(true)
                .show();

        return progress;
    }
}
